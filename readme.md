# Site d'un peintre

Entrainement à la realisation d'un projet complet de symfony 

## Environnement de développement 

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* Nnodejs et npm

Pour verifier les pré-requis (CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement 

```bash
composer install
npm install
npm run build
docker-compose up 
symfony serve -d
```

## Lancer tests

```bash
php bin/phpunit --testdox
```