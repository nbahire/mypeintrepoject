<?php

namespace App\Tests;

use App\Entity\Blogpost;
use DateTime;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $blogpost = new Blogpost();
        $datetime = new DateTime();

        $blogpost->setTitre('Titre')
             ->setContenu('Contenu')
             ->setSlug('slug')
             ->setCreatedAt($datetime)
        ;
        $this->assertTrue($blogpost->getTitre() === 'Titre');
        $this->assertTrue($blogpost->getContenu() === 'Contenu');
        $this->assertTrue($blogpost->getSlug() === 'slug');
        $this->assertTrue($blogpost->getCreatedAt() === $datetime);
    }

    public function testIsFalse()
    {
        $blogpost = new Blogpost();

        $blogpost->setTitre('Titre')
             ->setContenu('Contenu')
             ->setSlug('slug')
        ;
        $this->assertFalse($blogpost->getTitre() === 'Titre faux');
        $this->assertFalse($blogpost->getContenu() === 'Contenu faux');
        $this->assertFalse($blogpost->getSlug() === 'slugfaux');
        $this->assertFalse($blogpost->getCreatedAt() === new DateTime());
    }
    public function testIsEmpty()
    {
        $blogpost = new Blogpost();

        $this->assertEmpty($blogpost->getTitre());
        $this->assertEmpty($blogpost->getContenu());
        $this->assertEmpty($blogpost->getSlug());
        $this->assertEmpty($blogpost->getCreatedAt());
    }
}
