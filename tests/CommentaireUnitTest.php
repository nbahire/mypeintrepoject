<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new Blogpost();
        $peinture = new Peinture();


        $commentaire->setAuteur('Auteur')
             ->setEmail('email@test.test')
             ->setContenu('Contenu')
             ->setBlogpost($blogpost)
             ->setCreatedAt($datetime)
             ->setPeinture($peinture)
        ;
        $this->assertTrue($commentaire->getAuteur() === 'Auteur');
        $this->assertTrue($commentaire->getEmail() === 'email@test.test');
        $this->assertTrue($commentaire->getContenu() === 'Contenu');
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
        $this->assertTrue($commentaire->getCreatedAt() === $datetime);
        $this->assertTrue($commentaire->getPeinture() === $peinture);
    }

    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new Blogpost();
        $peinture = new Peinture();
        $commentaire->setAuteur('Auteur')
             ->setEmail('email@test.test')
             ->setContenu('Contenu')
             ->setBlogpost($blogpost)
             ->setPeinture($peinture)
        ;
        $this->assertFalse($commentaire->getAuteur() === 'Auteur faux');
        $this->assertFalse($commentaire->getEmail() === 'emailfaux@test.test');
        $this->assertFalse($commentaire->getContenu() === 'Contenu faux');
        $this->assertFalse($commentaire->getBlogpost() === new Blogpost());
        $this->assertFalse($commentaire->getCreatedAt() === new DateTime());
        $this->assertFalse($commentaire->getPeinture() === new Peinture());
    }
    public function testIsEmpty()
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getPeinture());
    }
}
