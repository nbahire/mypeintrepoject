<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Peinture;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class PeintureUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $peinture = new Peinture();
        $datetime = new DateTime();
        $categorie = new Categorie();
        $user = new User();


        $peinture->setNom('nom')
             ->setLargeur(10.10)
             ->setHauteur(10.10)
             ->setEnVente(true)
             ->setDateRealisation($datetime)
             ->setCreatedAt($datetime)
             ->setDescription('description')
             ->setPortefolio(true)
             ->setSlug('slug')
             ->setFile('File')
             ->addCategorie($categorie)
             ->setPrix(10.10)
             ->setUser($user)
        ;
        $this->assertTrue($peinture->getNom() === 'nom');
        $this->assertTrue($peinture->getLargeur() == 10.10);
        $this->assertTrue($peinture->getHauteur() == 10.10);
        $this->assertTrue($peinture->getEnVente() === true);
        $this->assertTrue($peinture->getDateRealisation() === $datetime);
        $this->assertTrue($peinture->getCreatedAt() === $datetime);
        $this->assertTrue($peinture->getDescription() === 'description');
        $this->assertTrue($peinture->getPortefolio() === true);
        $this->assertTrue($peinture->getSlug() === 'slug');
        $this->assertTrue($peinture->getFile() === 'File');
        $this->assertContains($categorie, $peinture->getCategorie());
        $this->assertTrue($peinture->getPrix() == 10.10);
        $this->assertTrue($peinture->getUser() === $user);
    }

    public function testIsFalse()
    {
        $peinture = new Peinture();
        $datetime = new DateTime();
        $categorie = new Categorie();
        $user = new User();

        $peinture->setNom('nom')
             ->setLargeur(10.10)
             ->setHauteur(10.10)
             ->setEnVente(true)
             ->setDateRealisation($datetime)
             ->setCreatedAt($datetime)
             ->setDescription('description')
             ->setPortefolio(true)
             ->setSlug('slug')
             ->setFile('File')
             ->addCategorie($categorie)
             ->setPrix(10.10)
             ->setUser($user)
        ;
        $this->assertFalse($peinture->getNom() === 'nom faux');
        $this->assertFalse($peinture->getLargeur() == 20.10);
        $this->assertFalse($peinture->getHauteur() == 20.10);
        $this->assertFalse($peinture->getEnVente() === false);
        $this->assertFalse($peinture->getDateRealisation() === new DateTime());
        $this->assertFalse($peinture->getCreatedAt() === new DateTime());
        $this->assertFalse($peinture->getDescription() === 'description faux');
        $this->assertFalse($peinture->getPortefolio() === false);
        $this->assertFalse($peinture->getSlug() === 'slug faux');
        $this->assertFalse($peinture->getFile() === 'File faux');
        $this->assertContains($categorie, $peinture->getCategorie());
        $this->assertFalse($peinture->getPrix() == 20.10);
        $this->assertFalse($peinture->getUser() === new User());
    }
    public function testIsEmpty()
    {
        $peinture = new Peinture();

        $this->assertEmpty($peinture->getNom());
        $this->assertEmpty($peinture->getLargeur());
        $this->assertEmpty($peinture->getHauteur());
        $this->assertEmpty($peinture->getEnVente());
        $this->assertEmpty($peinture->getDateRealisation());
        $this->assertEmpty($peinture->getCreatedAt());
        $this->assertEmpty($peinture->getDescription());
        $this->assertEmpty($peinture->getPortefolio());
        $this->assertEmpty($peinture->getSlug());
        $this->assertEmpty($peinture->getFile());
        $this->assertEmpty($peinture->getCategorie());
        $this->assertEmpty($peinture->getPrix());
        $this->assertEmpty($peinture->getUser());
    }
}
