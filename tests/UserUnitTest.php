<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user->setEmail('email@text.com')
             ->setPrenom('prenom')
             ->setNom('nom')
             ->setPassword('pass')
             ->setAPropos('a propos')
             ->setInstagram('instagram')
        ;
        $this->assertTrue($user->getEmail() === 'email@text.com');
        $this->assertTrue($user->getPrenom() === 'prenom');
        $this->assertTrue($user->getNom() === 'nom');
        $this->assertTrue($user->getPassword() === 'pass');
        $this->assertTrue($user->getAPropos() === 'a propos');
        $this->assertTrue($user->getInstagram() === 'instagram');
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('email@text.com')
             ->setPrenom('prenom')
             ->setNom('nom')
             ->setPassword('pass')
             ->setAPropos('a propos')
             ->setInstagram('instagram')
        ;
        $this->assertFalse($user->getEmail() === 'emailfaux@text.com');
        $this->assertFalse($user->getPrenom() === 'prenomfaux');
        $this->assertFalse($user->getNom() === 'nomfaux');
        $this->assertFalse($user->getPassword() === 'passfaux');
        $this->assertFalse($user->getAPropos() === 'a propos faux');
        $this->assertFalse($user->getInstagram() === 'instagramfaux');
    }
    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getInstagram());
    }
}
